import json

class config:
    with open("config.txt") as f: data = json.load(f)
    WIDTH = data["WIDTH"]
    HEIGHT = data["HEIGHT"]
    BOTPATH = data["BOTPATH"]


