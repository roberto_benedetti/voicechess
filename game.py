import chess

from voiceControl import voiceControl


class game():
    def __init__(self):
        self.P1Side = 'white' #self.sideSelector()
        # TODO: Implement start with black pieces
        self.inputMethod = self.inputMethod()
        self.isWhiteTurn = True # Flag to enstablish current turn
        self.move_list = list()
        self.chessboard = chess.Board()
        self.state = [self.chessboard]
        self.voiceControl = voiceControl()

    def sideSelector(self):
        print('Whelcome\n\tChoose a side: [white, black]')
        side = input().lower()
        if not ['white', 'black'].__contains__(side):
            return self.sideSelector()

    def inputMethod(self):
        print('Select an input method: [keyboard, voice]')
        method = input().lower()
        if not ['keyboard', 'voice'].__contains__(method):
            return self.inputMethod()
        else: return method

    def move(self, coordinates:str):
        '''
        Move current player piece
        - coordinates:str
            Moving coordinates formatted as starting position arrival position [column][number][column][number]
            eg: white pawn opening in E4 -> E2E4
        '''
        # TODO: Some interrupt caused by the window is making the listening method to interrupt and return None resultin in coordinate.lower to throw AttributeError
        try:
            self.chessboard.push_san(coordinates.lower())
            self.isWhiteTurn = not self.isWhiteTurn
            # Update game history
            self.state.append(self.chessboard)
            self.move_list.append(coordinates)
        except ValueError:
            print('Move not allowed')
        # Seems to be fixed with if command==None in move interpreter, to be tested
        # except AttributeError:
        #     pass
    def undo(self):
        """
        Remove last two moves (CPU and P1)
        """
        if len(self.state)>=2:
            self.move_list.pop()
            self.move_list.pop()
            self.chessboard.pop()
            self.chessboard.pop()
