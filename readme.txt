External requirements:

# PyQt5
sudo apt-get install python3-pyqt5  
sudo apt-get install pyqt5-dev-tools
sudo apt-get install qttools5-dev-tools
pip install pyqt5

# pyaudio

sudo apt install portaudio19-dev
sudo apt install python3-pyaudio
pip3 install pyaudio