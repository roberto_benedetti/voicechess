import chess
import chess.engine
import os
import random
from config import config

class abstractOpponent():
    def __init__(self, side = 'black'):
        self.side = side
        self.engine = None

    def move(self, board):
        result = self.engine.play(board, chess.engine.Limit(time=2*random.random())).move
        print('Opponent: My move is {}'.format(str(result)))
        return result



class easyOpponent(abstractOpponent):
    def __init__(self,side):
        super().__init__(side=side)
        basepath =os.path.dirname(__file__)
        # self.engine =  chess.engine.SimpleEngine.popen_uci(os.path.join(basepath,"stockfish_20090216_x64_modern.exe"))
        self.engine =  chess.engine.SimpleEngine.popen_uci(config.BOTPATH)

        elo = self.StrengthSelector()
        self.engine.configure({"UCI_LimitStrength": True, "UCI_Elo": elo})


    def StrengthSelector(self):
        print("Select your opponent ELO: (min: 1350, max: 2850)")
        elo = input()
        try:
            elo = int(elo)
            if ((elo < 1350) or (elo > 2850)):
                print("Invalid input")
                return self.StrengthSelector()
            else: return elo
        except Exception:
            print("Please insert a number")
            return self.StrengthSelector()

# ez = easyOpponent('black')
# print()