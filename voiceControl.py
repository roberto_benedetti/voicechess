import speech_recognition as sr
from gtts import gTTS
import re
from pydub import AudioSegment
from pydub.playback import play
import os

class voiceControl():
    def __init__(self):
        self.recognizer = sr.Recognizer()
        self.speech_engine = gTTS

    def listen(self):
        with sr.Microphone() as source:
            self.recognizer.adjust_for_ambient_noise(source)
            print("Ascolto: qual'è la tua mossa?")
            audio = self.recognizer.listen(source)
        try:
            text = self.recognizer.recognize_google(audio, language="it-IT")
            print("Google ha capito: \n", text)
            # self.talk(text=text)
            return text
        except Exception as e:
            print(e)

    def talk(self, text):
        self.speech_engine(text=text, lang = 'it').save('temp.mp3')
        song = AudioSegment.from_file('temp.mp3', format="mp3")
        play(song)
        os.remove('temp.mp3')

    def moveListener(self):
        try:
            command = self.listen()
            # If command is None (interrupt? Dunno but happens) then reinvoke the listener
            if command!=None:
                return moveInterpreter(command)
            else: return self.moveListener()
        except Exception:
            return ''

def moveInterpreter(text):
    # Post process voice commands into moveset
    # remove any é è
    s = text.lower().replace(' ', '')
    s = re.sub('[èé]', 'e', s)
    s = re.sub('ì', 'i', s)

    s = re.sub('[\W_]+', '', s)
    s = re.sub('effe','f',s)
    s = re.sub('di','d',s)
    s = re.sub('ci','c',s)
    s = re.sub('bi','b',s)
    s = re.sub('acca','h',s)


    s = re.sub('uno','1',
               re.sub('due','2',
                      re.sub('tre','3',
                             re.sub('quattro','4',
                                    re.sub('cinque','5',
                                           re.sub('sei','6',
                                                  re.sub('sette','7',
                                                         re.sub('otto','8',s))))))))
    if s == 'annulla':
        return s
    elif (re.match('[a-h][1-8][a-h][1-8]', s)!=None): return s
    else: return ''

