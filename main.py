from view import MainWindow
from PyQt5.QtWidgets import QApplication
from game import game
import threading
from bots.stockfish import easyOpponent

def threadGame(game, window):
        while (not isGameFinished):
                # If P1 turn make move else make bot move
                if game.isWhiteTurn:
                        print('Insert a move [A-H][1-8] [A-H][1-8]')
                        if game.inputMethod == 'keyboard':
                                command = input()
                        else:
                                command = game.voiceControl.moveListener()
                        if command == 'annulla':
                                game.undo()
                        else:
                                game.move(coordinates=command)

                else:
                        move = str(opponent.move(board=game.chessboard))
                        game.move(coordinates=move)
                        game.voiceControl.talk('Gioco {},{}'.format(move[:2],move[2:]))

                # Compute window refresh

                print(game.chessboard)
                window.update()



if __name__ == '__main__':
        # Init game board and side selection
        game = game()
        isGameFinished = False

        print(game.chessboard)

        # Init opponent
        # TODO: To implement with reading from game library , possibily move it to game constructor?
        opponentSide = 'black'
        opponent = easyOpponent(side=opponentSide)

        ########################################
        #       START VIEW & GAME THREAD       #
        ########################################

        app = QApplication([])
        window = MainWindow(board = game.chessboard)

        
        x = threading.Thread(target = threadGame, args = (game,window))
        x.start()

        window.show()
        app.exec()



