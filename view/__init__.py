import chess
import chess.svg

from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtWidgets import QApplication, QWidget

from config import config



class MainWindow(QWidget):
    def __init__(self, board):
        super().__init__()
        self.setGeometry(100, 100, config.HEIGHT, config.WIDTH)

        self.widgetSvg = QSvgWidget(parent=self)
        self.widgetSvg.setGeometry(0,0, config.WIDTH, config.HEIGHT)

        self.chessboard = board

        self.chessboardSvg = chess.svg.board(self.chessboard).encode("UTF-8")
        self.widgetSvg.load(self.chessboardSvg)

    def update(self):
         self.chessboardSvg = chess.svg.board(self.chessboard).encode("UTF-8")
         self.widgetSvg.load(self.chessboardSvg)

# if __name__ == "__main__":
#     window = MainWindow(chess.Board())
#     window.show()
#     app.exec()